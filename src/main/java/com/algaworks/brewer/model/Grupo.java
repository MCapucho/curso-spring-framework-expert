package com.algaworks.brewer.model;

public class Grupo {

	private Long id;
	private String nome;
	private Permissao permissoes;
	
	// Getters e Setters
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Permissao getPermissoes() {
		return permissoes;
	}
	
	public void setPermissoes(Permissao permissoes) {
		this.permissoes = permissoes;
	}
}
