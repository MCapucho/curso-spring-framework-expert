package com.algaworks.brewer.model;

public enum TipoPessoa {
	
	FISICA("Física"),
	JURIDICA("Jurídica");
	
	private String descricao;
	
	TipoPessoa(String descricao) {
		this.descricao = descricao;
	}
	
	// Getters
	public String getDescricao() {
		return descricao;
	}
}
